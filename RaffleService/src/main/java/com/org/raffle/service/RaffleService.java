package com.org.raffle.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.raffle.exception.RequestDateException;
import com.org.raffle.model.AllottedSpot;
import com.org.raffle.model.AvailableSpot;
import com.org.raffle.model.RequestReleaseBean;
import com.org.raffle.model.SpotRequest;
import com.org.raffle.repository.AllottedSpotRepository;
import com.org.raffle.repository.AvailableSpotRepository;
import com.org.raffle.repository.SpotRequestRepository;

@Service
public class RaffleService {
	
	@Autowired
	AvailableSpotRepository asRepo;
	
	@Autowired
	SpotRequestRepository srRepo;
	
	@Autowired
	AllottedSpotRepository allottedspotRepo;

	
	public String releaseSpot(RequestReleaseBean reqBean) {
		List<java.util.Date> dates = reqBean.getReleasefordates();
		dates.forEach(d -> saveAvailableSpot(reqBean.getspotid() , d));
		return "Spot released successfully";
	}
	public SpotRequest requestSpot(SpotRequest reqbean) {	
		if ((reqbean.getReqdate()).compareTo( new Date()) == 0 || (reqbean.getReqdate()).compareTo( new Date()) < 0 ) {
			throw new RequestDateException("Requested date should be after today.");
		}
		return srRepo.save(reqbean);
	}
	public List<AvailableSpot> getAvailableSpots() {
		// TODO Auto-generated method stub
		return asRepo.findAll();
	}
	
	private void saveAvailableSpot(long spotid, Date d) {
		
		AvailableSpot as = new AvailableSpot();
		as.setspotid(spotid);
		as.setAvailableon(d);
		asRepo.save(as);
	}
	public List<AllottedSpot> getAllottedSpots(long empid) {
		return allottedspotRepo.findByEmpid(empid);
	}

}
