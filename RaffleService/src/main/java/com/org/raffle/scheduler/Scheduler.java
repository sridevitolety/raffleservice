package com.org.raffle.scheduler;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.org.raffle.model.AllottedSpot;
import com.org.raffle.model.AvailableSpot;
import com.org.raffle.model.SpotRequest;
import com.org.raffle.repository.AllottedSpotRepository;
import com.org.raffle.repository.AvailableSpotRepository;
import com.org.raffle.repository.SpotRequestRepository;

public class Scheduler {
	
	@Autowired
	AvailableSpotRepository asRepo;
	
	@Autowired
	SpotRequestRepository srRepo;
	
	@Autowired
	AllottedSpotRepository allotedSpotRepo;
	
	 
    @Scheduled (cron="0 0 0 * * ?")
    public void allotSpots() {
        //Read all request(spotrequests)s and do allotment from available spots(availablespots)
    	List<AvailableSpot> aspots = asRepo.findByAvailableDate();
    	int spots = aspots.size();
    	List<SpotRequest> requests = srRepo.findByReqDate(spots);
    	
    	for(int i=0;i<requests.size();i++) {
    		AllottedSpot as = new AllottedSpot();
    		as.setEmpid(requests.get(i).getEmpid());
    		as.setspotid(aspots.get(i).getspotid());
    		as.setAllottedOn(new Date());
    		allotedSpotRepo.save(as);
    	}
    }
}

