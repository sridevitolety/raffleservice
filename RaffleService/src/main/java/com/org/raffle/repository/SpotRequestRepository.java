package com.org.raffle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.org.raffle.model.SpotRequest;

@Repository
public interface SpotRequestRepository extends JpaRepository<SpotRequest , Long>{
	
	@Query(value = "select * from spotrequest where reqdate = sysdate limit ?1", nativeQuery = true)
	List<SpotRequest> findByReqDate(int spots);

}
