package com.org.raffle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EnableScheduling
public class RaffleServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RaffleServiceApplication.class, args);
	}

}
