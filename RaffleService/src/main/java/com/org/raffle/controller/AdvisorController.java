package com.org.raffle.controller;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.org.raffle.service.RequestDateException;

@ControllerAdvice
public class AdvisorController extends ResponseEntityExceptionHandler{	
	
		@ExceptionHandler(RequestDateException.class)
		public ResponseEntity<Object> handleRequestDateException(Exception ex , WebRequest req){
			
			 Map<String, Object> body = new LinkedHashMap<>();
			 body.put("timestamp", LocalDateTime.now());
			 body.put("message", "Exception : Runtime : Requested date should be after today.");
	
			 return new ResponseEntity<>(body, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
			
		}
	
		@ExceptionHandler(Exception.class)
		public ResponseEntity<Object> handleAllExceptions(Exception ex , WebRequest req){
			
			 Map<String, Object> body = new LinkedHashMap<>();
			 body.put("timestamp", LocalDateTime.now());
			 body.put("message", "Generic Exception : Runtime");

			 return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
			
		}

}
