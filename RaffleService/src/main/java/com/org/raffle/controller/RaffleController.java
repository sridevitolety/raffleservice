package com.org.raffle.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.raffle.model.AllottedSpot;
import com.org.raffle.model.AvailableSpot;
import com.org.raffle.model.RequestReleaseBean;
import com.org.raffle.model.SpotRequest;
import com.org.raffle.service.RaffleService;

@RestController
@RequestMapping("/raffle")
public class RaffleController {
	
		@Autowired
		RaffleService raffleServ;
		
		@GetMapping("/availablespot")
		public List<AvailableSpot> getAvailableSpots() {
			return raffleServ.getAvailableSpots();
		}
		
	
		@PostMapping("/spotreleasemanagement")
		public String releaseSpot(@RequestBody RequestReleaseBean reqbean) {
			return raffleServ.releaseSpot(reqbean);
		}
		
		@PostMapping("/spotrequestmanagement")
		public SpotRequest requestSpot(@RequestBody SpotRequest reqbean) {
			return raffleServ.requestSpot(reqbean);
		}
		
		@GetMapping("/allottedspot/{empid}")
		public List<AllottedSpot> getAllottedSpots(@PathVariable long empid) {
			return raffleServ.getAllottedSpots(empid);
		}
}
