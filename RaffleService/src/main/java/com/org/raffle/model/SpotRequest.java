package com.org.raffle.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class SpotRequest {
	
	@Id
	@GeneratedValue
	private long id;
	private long empid;
	private Date reqdate;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getEmpid() {
		return empid;
	}
	public void setEmpid(long empid) {
		this.empid = empid;
	}
	public Date getReqdate() {
		return reqdate;
	}
	public void setReqdate(Date reqdate) {
		this.reqdate = reqdate;
	}
	public SpotRequest(long id, long empid, Date reqdate) {
		super();
		this.id = id;
		this.empid = empid;
		this.reqdate = reqdate;
	}
	public SpotRequest() {}

}
