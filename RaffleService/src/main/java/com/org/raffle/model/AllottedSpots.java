package com.org.raffle.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class AllottedSpot {
	
	@Id
	@GeneratedValue
	private long id;
	private long empid;
	private long spotid;
	private Date allottedOn;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getEmpid() {
		return empid;
	}
	public void setEmpid(long empid) {
		this.empid = empid;
	}
	public long getspotid() {
		return spotid;
	}
	public void setspotid(long spotid) {
		this.spotid = spotid;
	}
	public Date getAllottedOn() {
		return allottedOn;
	}
	public void setAllottedOn(Date allottedOn) {
		this.allottedOn = allottedOn;
	}
	public AllottedSpot(long id, long empid, long spotid, Date allottedOn) {
		super();
		this.id = id;
		this.empid = empid;
		this.spotid = spotid;
		this.allottedOn = allottedOn;
	}	
	public AllottedSpot() {}	

}
