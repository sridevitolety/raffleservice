package com.org.raffle.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class AvailableSpot {
	
	@Id
	@GeneratedValue
	private long id;
	private long spotid;
	private Date availableon;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getspotid() {
		return spotid;
	}
	public void setspotid(long spotid) {
		this.spotid = spotid;
	}
	public Date getAvailableon() {
		return availableon;
	}
	public void setAvailableon(Date availableon) {
		this.availableon = availableon;
	}
	public AvailableSpot(long id, long spotid, Date availableon) {
		super();
		this.id = id;
		this.spotid = spotid;
		this.availableon = availableon;
	}
	public AvailableSpot() {}
	

}
