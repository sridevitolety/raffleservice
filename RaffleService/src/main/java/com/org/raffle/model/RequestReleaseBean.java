package com.org.raffle.model;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class RequestReleaseBean {
	
	private long spotid;	
	private List<Date> releasefordates;
	
	
	public List<Date> getReleasefordates() {
		return releasefordates;
	}

	public void setReleasefordates(List<Date> releasefordates) {
		this.releasefordates = releasefordates;
	}

	public long getspotid() {
		return spotid;
	}

	public void setspotid(long spotid) {
		this.spotid = spotid;
	}
	/*public Date getFromDay() {
		return fromDay;
	}
	public void setFromDay(Date fromDay) {
		this.fromDay = fromDay;
	}

	public Date getToDay() {
		return toDay;
	}
	public void setToDay(Date toDay) {
		this.toDay = toDay;
	}	
	
	public RequestReleaseBean(long spotid, Date fromDay, Date toDay) {
		super();
		this.spotid = spotid;
		this.fromDay = fromDay;
		this.toDay = toDay;
	}
    */
	public RequestReleaseBean() {}

	public RequestReleaseBean(long spotid, List<Date> releasefordates) {
		super();
		this.spotid = spotid;
		this.releasefordates = releasefordates;
	}

	@Override
	public String toString() {
		return "RequestReleaseBean [spotid=" + spotid + ", releasefordates=" + releasefordates + "]";
	}
	
}
