package com.org.raffle.model;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class RequestSpotBean {
	
	private long empid;
	private Date requestedDay;	
	
	public Date getRequestedDay() {
		return requestedDay;
	}
	public void setRequestedDay(Date requestedDay) {
		this.requestedDay = requestedDay;
	}
	public long getEmpid() {
		return empid;
	}
	public void setEmpid(long empid) {
		this.empid = empid;
	}
	public RequestSpotBean(long empid, Date requestedDay) {
		super();
		this.empid = empid;
		this.requestedDay = requestedDay;
	}
	public RequestSpotBean() {}	
}
